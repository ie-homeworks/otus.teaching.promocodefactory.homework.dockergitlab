﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.Migrate();

            // Костыль для добавления тестовых данных 
            if (!_dataContext.Employees.Any())
            {
                _dataContext.AddRange(FakeDataFactory.Employees);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Preferences);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Customers);
                _dataContext.SaveChanges();
            }
        }
    }
}